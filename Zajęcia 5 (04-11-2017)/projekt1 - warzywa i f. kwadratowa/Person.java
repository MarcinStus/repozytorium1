public class Person {

    private String name;
    private String surname;
    private char gender;
    private String pesel;

    private Person mother;
    private Person father;

    public Person(String n, String s, char g, String p) {
        name = n;
        surname = s;
        setGender(g);
        setPesel(p);

        mother = null;
        father = null;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public char getGender() {
        return gender;
    }

    public String getPesel() {
        return pesel;
    }

    public Person getMother() {
        return mother;
    }

    public Person getFather() {
        return father;
    }

    public void setName(String n) {
        name = n;
    }

    public void setSurname(String s) {
        surname = s;
    }

    public void setGender(char g) {
        if (g == 'M' || g == 'm') {
            gender = 'M';
        }
        else if (g == 'F' || g == 'f') {
            gender = 'F';
        }
        else {
            gender = '-';
        }
    }

    public void setPesel(String p) {
        boolean acceptable = true;
        for (int i = 0; i < p.length(); i++) {
            if (p.charAt(i) < '0' || p.charAt(i) > '9') {
                acceptable = false;
            }
        }

        if (acceptable) {
            pesel = p;
        }
        else {
            pesel = "";
        }
    }

    public void setMother(Person m) {
        mother = m;
    }

    public void setFather(Person f) {
        father = f;
    }

    public String toString() {
        return name + " " + surname;
    }

    public String parents() {
        String result = "";
        if (mother != null) {
            result = result + "mother: " + mother + ", ";
        }
        else {
            result = result + "mother unknown, ";
        }

        if (father != null) {
            result = result + "father: " + father + ", ";
        }
        else {
            result = result + "father unknown, ";
        }
        return result;
    }



    public static void main(String[] args) {
        Person person1 = new Person("Jan", "Kowalski", 'M', "90122231145");
        Person person2 = new Person("Anna", "Nowak", 'F', "88122231145");
        Person person3 = new Person("Jan", "Nowak", 'M', "57122231145");
        Person person4 = new Person("Klaudia", "Nowak", 'F', "62122231145");

        person2.setMother(person4);
        person2.setFather(person3);

        System.out.println(person1 + " parents - " + person1.parents());
        System.out.println(person2 + " parents - " + person2.parents());
        System.out.println(person3 + " parents - " + person3.parents());
        System.out.println(person4 + " parents - " + person4.parents());
    }
}
