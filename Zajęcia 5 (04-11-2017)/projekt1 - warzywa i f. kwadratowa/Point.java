import java.util.Random;

public class Point {

    private Random generator = new Random();
    private double x, y;
    String name;

    public static int counter = -2;

    public Point(){
        this.x = 0;
        this.y = 0;
        counter++;
        setName("P"+counter);
    }

    public Point(double x , double y ){
        setX(x);
        setY(y);

        counter++;
        setName("P"+counter);
    }

    public Point(Point p){
        setY(p.y);
        setX(p.x);
        setName(p.name);

        counter++;
//        setName("P"+counter);
    }

    public Point(boolean f){
        randomize();

    }

    private void randomize(){
        this.setX(generator.nextInt(20) - 10 );
        this.setY(generator.nextInt(20) - 10);
        counter++;
        setName("P" + counter);
    }

    public  void set(int x, int y) {
        setX(x);
        setY(y);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public static int getCounter() {
        return counter;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return "{" + this.x + ", " + this.y + "}";
    }

    public boolean equals(Point p){
        return (this.x == p.x && this.y == p.y);
    }

    public double calculateDistance(Point p1, Point p2){
        return Math.round(Math.sqrt( Math.pow((p1.x - p2.x), 2) + Math.pow((p1.y - p2.y),2)) * 100d) / 100d;
    }

    public void move(int a , int b){
        this.x += a;
        this.y += b;
    }

    public int quarter(){
        if (x >= 0 && y >= 0){
            return 1;
        }
        else if (x < 0 && y >= 0){
            return 2;
        }
        else if (x < 0 && y < 0 ){
            return 3;
        }
        else{
            return   4;
        }
    }

    public static void show_points(Point p1, Point p2){

        for (int i = -11; i < 11 ; i++) {
            for (int j = -11; j < 11 ; j++) {
                if( (p1.x*-1 == i && p1.y ==j) || (p2.x*-1 == i && p2.y ==j)){
                    System.out.print("@");
                }
                else if( i == 0){
                    if( j == 0){
                        System.out.print("+");
                    }
                    else {
                        System.out.print("-");
                    }
                }
                else{
                    if(j == 0 ) {
                        System.out.print("|");
                    }
                    else{System.out.print(" ");}
                }
            }
            System.out.println();
        }
    }


}
