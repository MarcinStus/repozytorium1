import java.util.Scanner;

public class RegexParsers {

    public static boolean checkColor(String arg) {
        String regexColor = "Red|Green|Blue|Black|White";
        return arg.matches(regexColor) || arg.matches(regexColor.toUpperCase()) || arg.matches(regexColor.toLowerCase());
    }

    public static void testColor(Scanner cin) {
        String s;
        do {
            System.out.print("Wprowadź kolor: ");
            s = cin.nextLine();
            if (checkColor(s)) {
                System.out.println("Poprawnie. Dziękuję.");
            } else {
                System.out.println("Niepoprawnie. Spróbuj jeszcze raz!");
            }
        } while (!s.equals(""));
    }

    public static boolean checkInteger(String arg) {
        String regexInteger = "[-+]?[0-9]+";
        return arg.trim().replaceAll(" ", "").matches(regexInteger);
    }

    public static void testInteger(Scanner cin) {
        String s;
        do {
            System.out.print("Wprowadź liczbę całkowitą: ");
            s = cin.nextLine();
            if (checkInteger(s)) {
                System.out.println("Poprawnie. Dziękuję.");
            } else {
                System.out.println("Niepoprawnie. Spróbuj jeszcze raz!");
            }
        } while (!s.equals(""));
    }

    public static boolean checkDate(String arg) {
        String regexDate = "((([0-2]?[1-9])|([1-3]0)|31)[-./']((0?[1-9])|(1[0-2]))[-./']((19|20)[0-9]{2}))";
        return arg.matches(regexDate);
    }

    public static boolean checkDateWithoutRegex(String arg) {
        if (arg.length() < 10) return false;

        if (! ((arg.charAt(0) >= '0' && arg.charAt(0) <= '3') || arg.charAt(0) == ' ')) return false;
        if ((arg.charAt(0) == '0' || arg.charAt(0) == ' ') && arg.charAt(1) == '0') return false;
        if (! (arg.charAt(1) >= '0' && arg.charAt(1) <= '9')) return false;
        if (! (arg.charAt(2) == '/') || arg.charAt(2) == '.' || arg.charAt(2) == '-') return false;
        if (! ((arg.charAt(3) >= '0' && arg.charAt(3) <= '1') || arg.charAt(0) == ' ')) return false;
        if ((arg.charAt(3) == '0' || arg.charAt(3) == ' ') && arg.charAt(4) == '0') return false;
        if (! (arg.charAt(4) >= '0' && arg.charAt(4) <= '9')) return false;
        if (! (arg.charAt(5) == '/') || arg.charAt(5) == '.' || arg.charAt(5) == '-') return false;
        if (! (arg.charAt(6) == '1' || arg.charAt(6) == '2')) return false;
        if (! (arg.charAt(7) == '9' || arg.charAt(7) == '0')) return false;
        if (! (arg.charAt(8) >= '0' && arg.charAt(8) <= '9')) return false;
        if (! (arg.charAt(9) >= '0' && arg.charAt(9) <= '9')) return false;

        return true;
    }

    public static void testDate(Scanner cin) {
        String s;
        do {
            System.out.print("Wprowadź datę (DD/MM/YYYY): ");
            s = cin.nextLine();
            if (checkDate(s)) {
                System.out.println("Poprawnie. Dziękuję.");
            } else {
                System.out.println("Niepoprawnie. Spróbuj jeszcze raz!");
            }
        } while (!s.equals(""));
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        testDate(cin);
    }
}
