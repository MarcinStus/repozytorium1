import java.util.Scanner;

public class Student {

    private String name;
    private String surname;
    private int[] grades;

    public Student(String str) {
        String[] tab = str.split(" ");
        name = tab[0];
        surname = tab[1];
        if (tab.length == 3) {
            String[] gradesTab = tab[2].split(",");
            grades = new int[gradesTab.length];
            for (int i = 0; i < gradesTab.length; i++) {
                grades[i] = Integer.parseInt(gradesTab[i]);
            }
        }
        else {
            grades = new int[0];
        }
    }

    public String toString() {
        String result = name + " " + surname + " ";
        for (int i = 0; i < grades.length; i++) {
            result = result + grades[i];
            if (i != grades.length-1) {
                result = result + ",";
            }
        }
        return result;
    }


    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        String nameSurnameGradesRegex = "[A-ZŻŹŁĆŚ][a-zśćńąęłżźó]+ [A-ZŻŹŁĆŚ][a-zśćńąęłżźó]+( (([1-6],)*[1-6])?)?";
        String nameSurnameGrades;

        while (true) {
            nameSurnameGrades = cin.nextLine();
            if (nameSurnameGrades.matches(nameSurnameGradesRegex)) {
                Student s = new Student(nameSurnameGrades);
                System.out.println(s);
            }
            else {
                return;
            }
        }
    }
}
