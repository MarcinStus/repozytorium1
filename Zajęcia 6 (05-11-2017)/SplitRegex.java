import java.util.Scanner;

public class SplitRegex {

    public static String text = "Wpłynąłem na suchego przestwór oceanu,\n" +
            "Wóz nurza się w zieloność i jak łódka brodzi,\n" +
            "Śród fali łąk szumiących, śród kwiatów powodzi,\n" +
            "Omijam koralowe ostrowy burzanu.\n" +
            "\n" +
            "Już mrok zapada, nigdzie drogi ni kurhanu;\n" +
            "Patrzę w niebo, gwiazd szukam, przewodniczek łodzi;\n" +
            "Tam z dala błyszczy obłok - tam jutrzenka wschodzi;\n" +
            "To błyszczy Dniestr, to weszła lampa Akermanu.\n" +
            "\n" +
            "Stójmy! - jak cicho! - słyszę ciągnące żurawie,\n" +
            "Których by nie dościgły źrenice sokoła;\n" +
            "Słyszę, kędy się motyl kołysa na trawie,\n" +
            "\n" +
            "Kędy wąż śliską piersią dotyka się zioła.\n" +
            "W takiej ciszy - tak ucho natężam ciekawie,\n" +
            "Że słyszałbym głos z Litwy. - Jedźmy, nikt nie woła.";

    public static void main(String[] args) {
        String regex;
        Scanner cin = new Scanner(System.in);
        do {
            System.out.println("Wprowadź wyrażenie regularne: ");
            regex = cin.nextLine();

            String[] tab = text.split(regex);
            System.out.println("Ilość elementów: " + tab.length);
            for (int i = 0; i < tab.length; i++) {
                System.out.println(i + ": " + tab[i]);
            }

        } while (! regex.equals(""));
    }

}
