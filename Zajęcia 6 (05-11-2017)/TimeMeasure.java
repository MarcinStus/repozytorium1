import java.time.Duration;
import java.time.Instant;
import java.util.Locale;
import java.util.Scanner;

public class TimeMeasure {

    public static void main(String[] args) {
        Instant start = Instant.now();

        Scanner wejscie = new Scanner(System.in);
        wejscie.useLocale(Locale.US);   //zmiana systemu wprowadzania liczb dziesietnych - z kropka
        double liczba1, liczba2;

        System.out.print("Hej. Wprowadź pierwszą liczbę: ");
        liczba1 = wejscie.nextDouble();
        System.out.print("Teraz wprowadź drugą liczbę: ");
        liczba2 = wejscie.nextDouble();

        System.out.print("Co chcesz zrobić? (1-DOD, 2-ODJ, 3-MNO, 4-DZI) ");
        int dzialanie = wejscie.nextInt();

        switch (dzialanie) {
            case 1:
                System.out.println("" + liczba1 + " + " + liczba2 + " = " + (liczba1 + liczba2));
                break;
            case 2:
                System.out.println("" + liczba1 + " - " + liczba2 + " = " + (liczba1 - liczba2));
                break;
            case 3:
                System.out.println("" + liczba1 + " * " + liczba2 + " = " + (liczba1 * liczba2));
                break;
            case 4:
                System.out.println("" + liczba1 + " / " + liczba2 + " = " + (liczba1 / liczba2));
                break;
            default:
                System.out.println("Nie wybrałeś żadnego działania!");
                break;
        }

        Instant stop = Instant.now();
        Duration duration = Duration.between(start, stop);
        System.out.println("Czas wykonania: " + duration.getSeconds() + "sek., " + duration.getNano() + "nanosek.");

    }

}
