import java.util.Scanner;

class Thing{

    private String name;
    private int age;

    public Thing(String name,int age){
        this.name=name;
        this.age=age;
        System.out.println("to jest obiekt o nazwie "+name + " i wieku "+age);
    }
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }
    public void setName(String  name){
        this.name=name;
    }
    public void setAge(int age){
        this.age=age;
    }
    public String toString(){
        String result="to jest obiekt o nazwie "+name + " i wieku "+age;
        return result;
    }

}



public class Aplikacja {
    public static void main(String[] args) {
        Scanner cin =new Scanner(System.in);

        Thing t1 = new Thing("Kamil", 30);
        Thing t2,t3;
       t2 = new Thing("Zenon",35);
        t1.setName("Piotrek");
        t1.setAge(105);
        System.out.println(t1);
        System.out.println(t1.getAge());
       /* System.out.println(t1);
        System.out.println();
        System.out.println(t2);*/

        System.out.println("podaj nazwe dla obiektu ");
        t1.setName(cin.nextLine());
        System.out.println(t1);
    }


}
