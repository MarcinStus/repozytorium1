import java.util.Scanner;

public class Zadanie7 {

    public static void pobierz(int tab[][]) {

        ///////////////     POBIERANIE    ///////////////

        Scanner cin = new Scanner(System.in);
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                System.out.print("T["+i+"]["+j+"]");
                tab[i][j]=cin.nextInt();
            }
        }

        System.out.println();

        ///////////////     DRUKOWANIE    ///////////////

        for (int i = 0; i < tab.length; i++) {
            int a=0;
            for (int j = 0; j < tab[i].length; j++) {
                if (tab[i][j]>0){
                    a++;
                }
                if (a==tab[i].length){
                        for (int l = 0; l <tab[i].length ; l++) {
                            System.out.print(tab[i][l]);
                            if (l<tab[i].length-1){
                                System.out.print(",");
                            }
                        }
                        System.out.println();

                }

            }
        }
    }


    public static void main(String[] args) {

        int tab[][] = new int[3][3];
        pobierz(tab);
    }
}