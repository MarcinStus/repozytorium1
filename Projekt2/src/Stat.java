import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Stat {
    public static void main(String[] args) {
        Random gen = new Random();
        Scanner cin = new Scanner(System.in);
        int n;
        int[] T;
        System.out.println("podaj liczbę elementów tablicy: ");
        n =cin.nextInt();
        T = new int[n];
        System.out.println("podaj zakres losowania: ");
        int zakres = cin.nextInt();
        System.out.println("Losuję tablice");
        for (int i = 0; i <T.length; i++) {
            T[i]=gen.nextInt(zakres)+1;
        }
        System.out.print("T = [");
        for (int i = 0; i < T.length; i++) {
            System.out.print(T[i]);
            if (i < T.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");

        for (int i = 1; i < zakres+1; i++) {
            int licznik=0;
            for (int j=0;j<T.length;j++){
                if(T[j]==i)
                    licznik++;
            }
            System.out.println("wartosc: " + i +"  ilosc: " + licznik);
        }

    }
}