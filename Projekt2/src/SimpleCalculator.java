import java.util.InputMismatchException;
import java.util.Scanner;

public class SimpleCalculator {
    private double a, b;

    public static double suma(double a, double b) {
        return a + b;
    }

    public static double roznica(double a, double b) {
        return a - b;
    }

    public static double iloczyn(double a, double b) {
        return a * b;
    }

    public static double iloraz(double a, double b) {
        return a / b;
    }

    public static double potega(double a, double b) {
        return Math.pow(a, b);
    }

    public static double pierwiastek(double a) {
        return Math.sqrt(a);
    }


    public static void main(String[] args) {
        double a, b;
        Scanner cin = new Scanner(System.in);


        while (true) {
            try {

                System.out.println("Wprowadz a: ");
                a = cin.nextDouble();
                System.out.println("Wprowadz b: ");
                b = cin.nextDouble();

                System.out.println("a+b = " + suma(a, b));
                System.out.println("a-b = " + roznica(a, b));
                System.out.println("a*b = " + iloczyn(a, b));
                System.out.println("a/b = " + iloraz(a, b));
                System.out.println("a^b = " + potega(a,b));
                System.out.println("sqrt(a) = " + pierwiastek(a));
                System.out.println("sqrt(b) = " + pierwiastek(b));

            } catch (ArithmeticException e) {
                System.out.println("Błąd");
            } catch (InputMismatchException e1) {
                System.out.println("Błąd formatu wprowadzanych danych");
                cin.nextLine();
            }
        }
    }
}
