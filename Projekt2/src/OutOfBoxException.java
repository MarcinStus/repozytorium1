public class OutOfBoxException extends Exception {

    private int indexRange;

    public OutOfBoxException(int indexRande){
        this.indexRange=indexRande;
    }

    public  int getIndexRange(){
        return  indexRange;
    }

}
