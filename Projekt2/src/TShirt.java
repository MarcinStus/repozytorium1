import java.util.Scanner;

public class TShirt {

    public enum TShirtSizes { S, M, L, XL, XXL };
    public enum TShirtColors { White, Black, Blue, Red, Green };

    public static String availableSizes() {
        String result = "<";
        for (int i = 0; i < TShirtSizes.values().length; i++) {
            result = result + TShirtSizes.values()[i];
            if (i < TShirtSizes.values().length - 1) {
                result = result + ", ";
            }
        }
        result = result + ">";
        return result;
    }

    public static String availableColors() {
        String result = "<";
        for (TShirtColors color : TShirtColors.values()) {
            result = result + color + ", ";
        }
        result = result.substring(0, result.length() - 2);
        result = result + ">";
        return result;
    }

    private TShirtSizes size;
    private TShirtColors color;
    private String producer;
    private String model;

    public TShirt() {
        size = TShirtSizes.M;
        color = TShirtColors.Black;
        producer = "Made in China";
        model = "universal";
    }

    public TShirt(TShirtSizes size, TShirtColors color, String producer, String model) {
        this.size = size;
        this.color = color;
        this.producer = producer;
        this.model = model;
    }

    public TShirtSizes getSize() {
        return size;
    }

    public TShirtColors getColor() {
        return color;
    }

    public String getProducer() {
        return producer;
    }

    public String getModel() {
        return model;
    }

    public void setSize(TShirtSizes size) {
        this.size = size;
    }

    public void setColor(TShirtColors color) {
        this.color = color;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String toString() {
        return "" + size + " " + color + " " + producer + " model: " + model;
    }

    public boolean equals(TShirt t2) {
        if (size != t2.size) return false;
        if (color != t2.color) return false;
        if (! producer.equals(t2.producer)) return false;
        if (! model.equals(t2.model)) return false;
        return true;
    }


    //TESTOWANIE:
    public static void main(String[] args) {
        TShirt t1 = new TShirt();
        TShirt t2 = new TShirt(TShirtSizes.XXL, TShirtColors.Green, "Made in USA", "big");
        System.out.println(t1);
        System.out.println(t2);

        Scanner in = new Scanner(System.in);
        String size, color, producer, model;

        System.out.println("Twoja wymarzona koszulka:");
        System.out.print("Rozmiar " + availableSizes() + ": ");
        size = in.nextLine();
        System.out.print("Kolor " + availableColors() + ": ");
        color = in.nextLine();
        System.out.print("Producent: ");
        producer = in.nextLine();
        System.out.print("Model: ");
        model = in.nextLine();

        size = size.toUpperCase();
        color = color.substring(0, 1).toUpperCase() + color.substring(1).toLowerCase();

        TShirt t3 = new TShirt(TShirtSizes.valueOf(size), TShirtColors.valueOf(color), producer, model);
        System.out.println(t3);


        System.out.println("Twoja druga wymarzona koszulka:");
        System.out.print("Rozmiar " + availableSizes() + ": ");
        size = in.nextLine();
        System.out.print("Kolor " + availableColors() + ": ");
        color = in.nextLine();
        System.out.print("Producent: ");
        producer = in.nextLine();
        System.out.print("Model: ");
        model = in.nextLine();

        size = size.toUpperCase();
        color = color.substring(0, 1).toUpperCase() + color.substring(1).toLowerCase();

        TShirt t4 = new TShirt(TShirtSizes.valueOf(size), TShirtColors.valueOf(color), producer, model);
        System.out.println(t4);

        System.out.println();

        if (t3.equals(t4)) {
            System.out.println("To ta sama koszulka!");
        }
    }
}
