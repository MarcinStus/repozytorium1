import javax.swing.*;
import java.awt.*;

public class TestComponents extends JFrame {

    public TestComponents() {
        super("Test komponentów");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);
        setLocation(100, 100);
        setLayout(new GridLayout(5, 5));

        JLabel napis = new JLabel("Cześć!");
        JTextField tekst = new JTextField("Tekst domyślny...");
        JCheckBox poleWyboru1 = new JCheckBox("Opcja 1");
        JCheckBox poleWyboru2 = new JCheckBox("Opcja 2");
        JCheckBox poleWyboru3 = new JCheckBox("Opcja 3");

        JPanel panel = new JPanel();
        JRadioButton poleWyboruAlternatywnego1 = new JRadioButton("Opcja 1");
        JRadioButton poleWyboruAlternatywnego2 = new JRadioButton("Opcja 2");
        JRadioButton poleWyboruAlternatywnego3 = new JRadioButton("Opcja 3");
        ButtonGroup wyborAlternatywny = new ButtonGroup();
        wyborAlternatywny.add(poleWyboruAlternatywnego1);
        wyborAlternatywny.add(poleWyboruAlternatywnego2);
        wyborAlternatywny.add(poleWyboruAlternatywnego3);
        panel.add(poleWyboruAlternatywnego1);
        panel.add(poleWyboruAlternatywnego2);
        panel.add(poleWyboruAlternatywnego3);

        add(napis);
        add(tekst);
        add(poleWyboru1);
        add(poleWyboru2);
        add(poleWyboru3);
        add(panel);

        setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TestComponents();
            }
        });
    }
}
