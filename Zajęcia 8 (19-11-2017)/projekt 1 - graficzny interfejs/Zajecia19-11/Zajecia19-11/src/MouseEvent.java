import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseMotionListener;

public class MouseEvent extends JFrame implements MouseMotionListener {

    public static final int SIZE = 1000;

    Point[] points;
    int i = 0;

    private class DrawingPanel extends JPanel {
        public DrawingPanel(int width, int height) {
            super();
            setPreferredSize(new Dimension(width, height));
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.RED);

            for (int i = 0; i < SIZE; i++) {
                if (points[i] == null) return;

                g2d.drawRect((int)points[i].getX()-14, (int)points[i].getY()-38, 1, 1);
            }
        }
    }

    DrawingPanel drw;

    public MouseEvent() {
        super( "Rysowanie myszką" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(50,50);
        setLayout(new FlowLayout());

        drw = new DrawingPanel(400, 400);
        addMouseMotionListener(this);
        add(drw);

        points = new Point[SIZE];
        for (int i = 0; i < SIZE; i++) {
            points[i] = null;
        }

        pack();
        setVisible(true);
    }

    @Override
    public void mouseDragged(java.awt.event.MouseEvent e) {
        points[i] = e.getPoint();
        i++;

        if (i == SIZE) {
            i = 0;
        }

        drw.repaint();
    }

    @Override
    public void mouseMoved(java.awt.event.MouseEvent e) {

    }


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MouseEvent();
            }
        });
    }

}
