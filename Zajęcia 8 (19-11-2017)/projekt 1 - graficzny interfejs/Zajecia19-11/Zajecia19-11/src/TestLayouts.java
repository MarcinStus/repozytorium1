import javax.swing.*;
import java.awt.*;

public class TestLayouts extends JFrame {

    public TestLayouts(int layoutChoice) {
        super("Test układu komponentów");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);
        setLocation(100, 100);

        switch (layoutChoice) {
            case 1:
                setLayout(new FlowLayout(FlowLayout.LEFT));
                break;
            case 2:
                setLayout(new FlowLayout(FlowLayout.CENTER));
                break;
            case 3:
                setLayout(new FlowLayout(FlowLayout.RIGHT));
                break;
            case 4:
                setLayout(new GridLayout(5, 5));
                break;
        }

        JLabel napis = new JLabel("Cześć!");
        JTextField tekst = new JTextField("Tekst domyślny...");
        JCheckBox poleWyboru1 = new JCheckBox("Opcja 1");
        JCheckBox poleWyboru2 = new JCheckBox("Opcja 2");
        JCheckBox poleWyboru3 = new JCheckBox("Opcja 3");

        for (int i = 0; i < 12; i++) {
            Button b = new Button("" + (i+1));
            b.setPreferredSize(new Dimension(50, 50));
            add(b);
        }

        add(napis);
        add(tekst);
        add(poleWyboru1);
        add(poleWyboru2);
        add(poleWyboru3);

        setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TestLayouts(2);
            }
        });
    }
}
