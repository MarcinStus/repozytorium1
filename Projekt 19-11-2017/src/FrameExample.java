import javax.swing.*;
import java.awt.*;

public class FrameExample {
    public static void main(String[] args) {
        JFrame window= new JFrame("Hello World!");
        window.setSize(400, 600);
        JButton b = new JButton("Clickme, please!");
        b.setSize(100,100);
        JButton b2 = new JButton("Clickme, too");
        b2.setSize(200,200);
        window.add(b);
        window.add(b2);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);}
}
