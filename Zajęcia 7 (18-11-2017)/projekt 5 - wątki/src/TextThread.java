import java.util.Random;

public class TextThread implements Runnable {

    private String message;
    private int delay;
    private boolean finalize;

    public TextThread(String message, int delay) {
        this.message = message;
        this.delay = delay;
        finalize = false;
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(delay);
            }
            catch (Exception e) {}

            if (finalize) return;
            System.out.println(message);
        }
    }

    public void terminate() {
        finalize = true;
    }


    public static void main(String[] args) {

        Random random = new Random();

        TextThread[] T = new TextThread[4];
        Thread[] TH = new Thread[4];

        for (int i = 0; i < 4; i++) {
            T[i] = new TextThread("Wątek " + i, (i+1) * 1000);      //1 w 1s 2 w 2 sekundy
            TH[i] = new Thread(T[i]);
            TH[i].start();
        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {}

        for (int i = 0; i < 4; i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {}
            T[i].terminate();
        }
    }

}
