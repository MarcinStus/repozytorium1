public class BalanceThread extends Thread {

    public static Integer number = 0;

    public synchronized static int getBalance() {
        number++;
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        number--;
        return number;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            int b = getBalance();
            System.out.println("Wartość: " + b);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread[] t = new BalanceThread[50];
        for (int i = 0; i < t.length; i++) {
            Thread.sleep(100);
            t[i] = new BalanceThread();
            t[i].start();
        }
    }

}
