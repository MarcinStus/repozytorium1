public class Czlowiek implements Zwierze {
    double masa;

    public String jedz() {
        return "Jem kanapkę z salami...";
    }

    public Czlowiek rozmnozSie() {
        Czlowiek malyCzlowiek = new Czlowiek();
        malyCzlowiek.masa = 500;
        return malyCzlowiek;
    }

    public void rosnij(double masa) {
        System.out.println("Rosnę o " + masa);
        this.masa += masa;
    }

    public void piszcz() {
        System.out.print("Aaaaa Aaa AAAAAA");
    }
}
