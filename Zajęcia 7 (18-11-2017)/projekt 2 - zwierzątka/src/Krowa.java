public class Krowa implements Zwierze {
    double masa;

    public String jedz() {
        return "Jem smaczną trawę...";
    }

    public Krowa rozmnozSie() {
        Krowa malaKrowa = new Krowa();
        malaKrowa.masa = 500;
        return malaKrowa;
    }

    public void rosnij(double masa) {
        System.out.println("Rosnę o " + masa);
        this.masa += masa;
    }
}
