import java.util.InputMismatchException;
import java.util.Scanner;

public class SimpleCalculator {

    public static double add (double a, double b) {
        return a+b;
    }

    public static double subtract (double a, double b) {
        return a-b;
    }

    public static double multiply (double a, double b) {
        return a*b;
    }

    public static double divide (double a, double b) {
        return a/b;
    }

    public static double power(double a, double power) {
        return Math.pow(a, power);
    }

    public static double sqrt(double a) {
        return Math.sqrt(a);
    }


    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        double a, b;

        while (true) {
            try {
                System.out.print("a = ");
                a = cin.nextDouble();
                System.out.print("b = ");
                b = cin.nextDouble();

                System.out.println("a + b = " + add(a, b));
                System.out.println("a - b = " + subtract(a, b));
                System.out.println("a * b = " + multiply(a, b));
                System.out.println("a / b = " + divide(a, b));
                System.out.println("a^b = " + power(a, b));
                System.out.println("sqrt(a) = " + sqrt(a));
                System.out.println("sqrt(b) = " + sqrt(b));
            } catch (ArithmeticException e) {
                System.out.println("Błąd obliczeń.");
            } catch (InputMismatchException e) {
                System.out.println("Błąd formatu wprowadzonych danych.");
                cin.nextLine();
            }
        }
    }

}
