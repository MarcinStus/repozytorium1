public class OutOfBoxException extends Exception {

    private int indexRange;

    public OutOfBoxException(int indexRange) {
        this.indexRange = indexRange;
    }

    public int getIndexRange() {
        return indexRange;
    }
}
