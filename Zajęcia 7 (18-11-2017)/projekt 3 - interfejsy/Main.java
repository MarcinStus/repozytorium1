import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);
        Computation dzialanie;
        double a, b;
        int wybor;

        while (true) {
            try {
                System.out.print("Jakie działanie Cię interesuje? <1 - mnożenie, 2 - potęgowanie> ");
                wybor = cin.nextInt();

                if (wybor == 1) {
                    dzialanie = new Multiplication();
                } else if (wybor == 2) {
                    dzialanie = new Exponentiation();
                } else {
                    dzialanie = null;
                }

                System.out.print("Czynnik A: ");
                a = cin.nextDouble();
                System.out.print("Czynnik B: ");
                b = cin.nextDouble();

                System.out.println("Wynik: " + dzialanie.compute(a, b));
            } catch (InputMismatchException e) {
                System.out.println("Błąd wprowadzonych danych. Spróbuj jeszcze raz.");
                cin.nextLine();
            } catch (NullPointerException e) {
                System.out.println("Działanie spoza dostępnych. Spróbuj jeszcze raz.");
            }
        }
    }

}
