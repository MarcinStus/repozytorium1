import java.util.Locale;
import java.util.Scanner;

public class Calculator4 {

    public static void main(String[] args) {
        Scanner wejscie = new Scanner(System.in);
        wejscie.useLocale(Locale.US);   //zmiana systemu wprowadzania liczb dziesietnych - z kropka
        double liczba1, liczba2;

        System.out.print("Hej. Wprowadź pierwszą liczbę: ");
        liczba1 = wejscie.nextDouble();
        System.out.print("Teraz wprowadź drugą liczbę: ");
        liczba2 = wejscie.nextDouble();

        System.out.print("Co chcesz zrobić? (1-DOD, 2-ODJ, 3-MNO, 4-DZI) ");
        int dzialanie = wejscie.nextInt();

        if (dzialanie == 1) {
            System.out.println("" + liczba1 + " + " + liczba2 + " = " + (liczba1 + liczba2));
        }
        else if (dzialanie == 2) {
            System.out.println("" + liczba1 + " - " + liczba2 + " = " + (liczba1 - liczba2));
        }
        else if (dzialanie == 3) {
            System.out.println("" + liczba1 + " * " + liczba2 + " = " + (liczba1 * liczba2));
        }
        else if (dzialanie == 4) {
            System.out.println("" + liczba1 + " / " + liczba2 + " = " + (liczba1 / liczba2));
        }
        else {
            System.out.println("Brak wybranego działania");
        }

        //przy wypisywaniu, zaczynając od "" (pusty łańcuch znaków) wymuszamy sklejanie napisów
        //wejscie.nextLine(); - wyczysci bufor z ewentualnych pozostałości
        //          (użyteczne gdy po wprowadzaniu liczb zaczynamy wprowadzać napisy)
    }

}
