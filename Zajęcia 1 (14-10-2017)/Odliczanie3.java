public class Odliczanie3 {

    public static void odlicz1(int ile) {
        for (int i = 1; i <= ile; i++) {
            System.out.println("Instrukcja nr " + i + ".");
        }
    }

    public static void odlicz2(int ile) {
        for (int i = ile; i > 0 ; i--) {
            System.out.println("Pozostało " + i + "instrukcji!");
        }
    }

    public static void main(String[] args) {

        odlicz1(5);
        odlicz2(5);

        System.out.println();

        odlicz1(12);
        odlicz2(10);

    }

}
