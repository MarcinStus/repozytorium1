import java.util.Scanner;

public class Odliczanie {

    public static void main(String[] args) {
        Scanner wejscie = new Scanner(System.in);
        System.out.print("Ile instrukcji chcesz wykonać? ");
        int liczba = wejscie.nextInt();

        //ROZWIĄZANIE 1
        for (int i = liczba; i > 0; i--) {
            System.out.println("do zakończenia pozostało " + i + " instrukcji");
        }

        System.out.println();

        //ROZWIĄZANIE 2
        for (int i = 0; i < liczba; i++) {
            System.out.println("do zakończenia pozostało " + (liczba-i) + " instrukcji");
        }

        System.out.println();

        //ROZWIĄZANIE 3 - za pomocą pętli WHILE
        int i = liczba;
        while (i > 0) {
            System.out.println("do zakończenia pozostało " + i + " instrukcji");
            i--;
        }

    }
}
