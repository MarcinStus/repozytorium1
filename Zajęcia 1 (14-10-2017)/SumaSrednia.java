import java.util.Scanner;

public class SumaSrednia {

    public static void main(String[] args) {

        Scanner wejscie = new Scanner(System.in);
        int liczba, suma = 0, min = 0, max = 0;

        for (int i = 0; i < 10; i++) {
            System.out.print("Wprowadź " + (i+1) + "-tą liczbę: ");
            liczba = wejscie.nextInt();

            if (i == 0) {
                min = liczba;
                max = liczba;
            }
            else {
                if (liczba < min) {
                    min = liczba;
                }
                if (liczba > max) {
                    max = liczba;
                }
            }

            suma = suma + liczba;
        }

        System.out.println("Suma wprowadzonych liczb: " + suma);
        System.out.println("Średnia wprowadzonych liczb: " + (double)suma / 10.0);
        System.out.println("Minimum wprowadzonych liczb: " + min);
        System.out.println("Maximum wprowadzonych liczb: " + max);

    }
}
