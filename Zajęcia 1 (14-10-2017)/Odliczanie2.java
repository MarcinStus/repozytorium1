public class Odliczanie2 {

    public static void odlicz1() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Instrukcja nr " + i + ".");
        }
    }

    public static void odlicz2() {
        for (int i = 10; i > 0 ; i--) {
            System.out.println("Pozostało " + i + "instrukcji!");
        }
    }

    public static void main(String[] args) {
        System.out.println("Cześć!");
        odlicz1();
        System.out.println("Chwila przerwy...");
        odlicz2();

        System.out.println();
        odlicz1();
        odlicz1();
        odlicz2();
        odlicz2();
    }

}
