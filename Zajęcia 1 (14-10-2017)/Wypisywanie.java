public class Wypisywanie {

    public static void wypisz(int ile, String co) {
        for (int i = 0; i < ile; i++) {
            System.out.println(co);
        }
    }

    public static void main(String[] args) {
        wypisz(5, "cześć");
        wypisz(3, "co słychać?");
        wypisz(10, "do zobaczenia!");
    }

}
