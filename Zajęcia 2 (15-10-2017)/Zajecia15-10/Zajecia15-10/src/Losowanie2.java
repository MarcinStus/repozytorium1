import java.util.Random;
import java.util.Scanner;

public class Losowanie2 {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Random gen = new Random();

        int n;
        int liczby[];

        System.out.print("Ile chciałbyś liczb? ");
        n = cin.nextInt();

        liczby = new int[n];

        for (int i = 0; i < n; i++) {
            liczby[i] = gen.nextInt(101);
            System.out.println("liczby[" + i + "] = " + liczby[i]);
        }

        int suma = 0, parz = 0, nparz = 0;
        int min = liczby[0], max = liczby[0];

        for (int i = 0; i < n; i++) {
            suma = suma + liczby[i];

            if (liczby[i] % 2 == 0) {
                parz++;
            }
            else {
                nparz++;
            }

            if (liczby[i] < min) {
                min = liczby[i];
            }
            if (liczby[i] > max) {
                max = liczby[i];
            }
        }

        System.out.println("Suma elementów: " + suma);
        System.out.println("Ilość parzystych: " + parz);
        System.out.println("Ilość nieparzystych: " + nparz);
        System.out.println("Minimum: " + min);
        System.out.println("Maksimum: " + max);
    }

}
