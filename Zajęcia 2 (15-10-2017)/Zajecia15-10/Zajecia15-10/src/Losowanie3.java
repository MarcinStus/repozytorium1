import java.util.Random;

public class Losowanie3 {

    public static void main(String[] args) {

        Random gen = new Random();

        int[] tablica1, tablica2, tablica3;
        tablica1 = new int[10];
        tablica2 = new int[10];
        tablica3 = new int[10];

        for (int i = 0; i < 10; i++) {
            tablica1[i] = gen.nextInt(21);
            tablica2[i] = gen.nextInt(21);
        }

        for (int i = 0; i < 10; i++) {
            tablica3[i] = tablica1[i] + tablica2[i];
        }

        for (int i = 0; i < 10; i++) {
            System.out.print("tablica3[" + i + "] = " + tablica3[i]);
            System.out.println(" ( " + tablica1[i] + " + " + tablica2[i] + " )");
        }
    }
}
