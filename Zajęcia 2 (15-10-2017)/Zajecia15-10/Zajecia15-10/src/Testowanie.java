import java.util.Locale;
import java.util.Scanner;

public class Testowanie {

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);
        cin.useLocale(Locale.US);
        double x, y;

        System.out.println("Wprowadź A: ");
        x = cin.nextDouble();
        System.out.println("Wprowadź B: ");
        y = cin.nextDouble();

        System.out.println("SUMA: " + Obliczenia.suma(x, y));
        System.out.println("ŚREDNIA: " + Obliczenia.srednia(x, y));
        System.out.println("MINIMUM: " + Obliczenia.minimum(x, y));
        System.out.println("MAXIMUM: " + Obliczenia.maximum(x, y));

        System.out.println();
        System.out.println("---------");
        System.out.println();

        System.out.print("Ile elementów tablicy? ");
        int n = cin.nextInt();
        double[] tablica = new double[n];

        Tablice.losuj(tablica);
        Tablice.wypisz(tablica);

        System.out.println("SUMA: " + Tablice.suma(tablica));
        System.out.println("ŚREDNIA: " + Tablice.srednia(tablica));
        System.out.println("MINIMUM: " + Tablice.minimum(tablica));
        System.out.println("MAXIMUM: " + Tablice.maximum(tablica));

        System.out.println();
        System.out.println("---------");
        System.out.println();

        double[][] macierz = new double[6][8];
        TabliceDwuwymiarowe.losuj(macierz);
        TabliceDwuwymiarowe.wypisz(macierz);
    }

}
