import java.util.Random;
import java.util.Scanner;

public class Losowanie1 {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Random gen = new Random();

        int n;
        int liczby[];

        System.out.print("Ile chciałbyś liczb? ");
        n = cin.nextInt();

        liczby = new int[n];

        for (int i = 0; i < n; i++) {
            liczby[i] = gen.nextInt(101);
        }

        for (int i = 0; i < n; i++) {
            System.out.println("liczby[" + i + "] = " + liczby[i]);
        }
    }
}
