import java.util.Scanner;

public class CiagTablicy {

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);
        int[] elementy = new int[20];

        System.out.print("Wprowadź 1-szy element: ");
        elementy[0] = cin.nextInt();
        System.out.print("Wprowadź 2-gi element: ");
        elementy[1] = cin.nextInt();

        for (int i = 2; i < elementy.length; i++) {
            elementy[i] = elementy[i-1] + elementy[i-2];
        }

        System.out.println("Zawartość tablicy:");
        for (int i = 0; i < elementy.length; i++) {
            System.out.print(elementy[i] + ", ");
        }
    }

}
