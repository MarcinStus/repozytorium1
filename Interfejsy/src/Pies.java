public class Pies implements Zwierze {
    double masa;

    public String jedz(){
        return "Jem ziarenka...";
    }
    public Pies rozmnozSie(){
        Pies malyPies = new Pies();
        malyPies.masa=100;
        return malyPies;
    }

    public void rosnij(double masa){
        System.out.println("Rosnę o masę: "+masa);
        this.masa +=masa;
    }

}
