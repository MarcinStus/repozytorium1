import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);

        Multiplication multi1 = new Multiplication();
        Exponentiation exp1 = new Exponentiation();

        System.out.println("Podaj a: ");
        double a = cin.nextDouble();
        System.out.println("Podaj b: ");
        double b = cin.nextDouble();

        System.out.println("a*b= "+multi1.compute(a,b));
        System.out.println("a^b= "+exp1.compute(a,b));

    }
}
