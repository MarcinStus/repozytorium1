public class Mysz implements Zwierze {
    double masa;

    public String jedz() {
        return "Jem ziarenka...";
    }

    public Mysz rozmnozSie() {
        Mysz malaMysz = new Mysz();
        malaMysz.masa = 100;
        return malaMysz;
    }

    public void rosnij(double masa) {
        System.out.println("Rosnę o masę: " + masa);
        this.masa += masa;
    }

    public void piszcz() {
        System.out.println("piiiii");
    }
}
