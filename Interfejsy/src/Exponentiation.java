public class Exponentiation implements Computation{
    public double compute(double a, double b) {
        return Math.pow(a, b);
    }
}
