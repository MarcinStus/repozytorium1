public class XPoint {

    private int x;
    private int y;

    private static int min = -20;
    private static int max = 20;

    public XPoint() {
        x = 0;
        y = 0;
    }

    public XPoint(int newX, int newY) {
        setX(newX);
        setY(newY);
    }

    public XPoint(XPoint p) {
        setX(p.x);
        setY(p.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int newX) {
        x = newX;
        if (x > max) {
            x = max;
        } else if (x < min) {
            x = min;
        }
    }

    public void setY(int newY) {
        y = newY;
        if (y > max) {
            y = max;
        } else if (y < min) {
            y = min;
        }
    }

    public void set(int newX, int newY) {
        setX(newX);
        setY(newY);
    }

    public static void setMin(int min) {
        XPoint.min = min;
    }

    public static void setMax(int max) {
        XPoint.max = max;
    }

    public String toString() {
        String result = "{" + x + ", " + y + "}";
        return result;
    }

    public boolean equals(XPoint p) {
        boolean result;
        if (x == p.x && y == p.y) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public void movePoint(int mx, int my) {
        x = x + mx;
        if (x > max) {
            x = max;
        } else if (x < min) {
            x = min;
        }

        y = y + my;
        if (y > max) {
            y = max;
        } else if (y < min) {
            y = min;
        }
    }

    public void clear() {
        x = 0;
        y = 0;
    }

    public double distance(XPoint p) {
        double result = (p.x - x) * (p.x - x);
        result = result + (p.y - y) * (p.y - y);
        result = Math.sqrt(result);
        return result;
    }

    public int quarter() {
        if (x >= 0 && y >= 0) {
            return 1;
        } else if (x < 0 && y >= 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else {
            return 4;
        }
    }


    public static void main(String[] args) {
        XPoint p1, p2;
        p1 = new XPoint(-25, 5);
        p2 = new XPoint(25, 30);

        XPoint.setMin(-5);              //dla zmiennych static możemy się odwolywac zarowno przez nazwe klasy jak i przez obiekt np. p1.setMin, p2.setMin, XPoint.setMin
        p2.setY(-10);

        System.out.println(p1);
        System.out.println(p2);
    }
}
