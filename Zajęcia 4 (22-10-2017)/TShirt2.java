import java.util.Scanner;

public class TShirt2 {

    public enum TShirtSizes {
        None(0), S(5), M(7), L(10), XL(12), XXL(16);

        private int inchSize;

        private TShirtSizes(int inch) {
            inchSize = inch;
        }

        public int getInchSize() {
            return inchSize;
        }
    }

    public enum TShirtColors { White, Red, Green, Blue, Black };

    private TShirtSizes size;
    private TShirtColors color;
    private String producer;
    private String model;

    public TShirt2() {
        size = TShirtSizes.M;
        color = TShirtColors.Black;
        producer = "simple";
        model = "default";
    }

    public TShirt2(TShirtSizes s, TShirtColors c, String p, String m) {
        size = s;
        color = c;
        producer = p;
        model = m;
    }

    public TShirtSizes getSize() {
        return size;
    }

    public TShirtColors getColor() {
        return color;
    }

    public String getProducer() {
        return producer;
    }

    public String getModel() {
        return model;
    }

    public void setSize(TShirtSizes size) {
        this.size = size;
    }

    public void setColor(TShirtColors color) {
        this.color = color;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String toString() {
        return "" + size + "(" + size.getInchSize() + ") " + color + " " + producer + " model: " + model;
    }

    public boolean equals(TShirt2 t2) {
        if (size != t2.size) return false;
        if (color != t2.color) return false;
        if (! producer.equals(t2.producer)) return false;
        if (! model.equals(t2.model)) return false;
        return true;
    }

    //TESTOWANIE:
    public static void main(String[] args) {
        TShirt2 t1 = new TShirt2();
        TShirt2 t2 = new TShirt2(TShirt2.TShirtSizes.XXL, TShirt2.TShirtColors.Green, "Made in USA", "big");
        System.out.println(t1);
        System.out.println(t2);

        Scanner in = new Scanner(System.in);
        String size, color, producer, model;

        System.out.println("Twoja wymarzona koszulka:");
        System.out.print("Rozmiar: ");
        size = in.nextLine();
        System.out.print("Kolor: ");
        color = in.nextLine();
        System.out.print("Producent: ");
        producer = in.nextLine();
        System.out.print("Model: ");
        model = in.nextLine();

        size = size.toUpperCase();
        color = color.substring(0, 1).toUpperCase() + color.substring(1).toLowerCase();

        TShirt2 t3 = new TShirt2(TShirt2.TShirtSizes.valueOf(size), TShirt2.TShirtColors.valueOf(color), producer, model);
        System.out.println(t3);

        System.out.println("Twoja druga wymarzona koszulka:");
        System.out.print("Rozmiar: ");
        size = in.nextLine();
        System.out.print("Kolor: ");
        color = in.nextLine();
        System.out.print("Producent: ");
        producer = in.nextLine();
        System.out.print("Model: ");
        model = in.nextLine();

        size = size.toUpperCase();
        color = color.substring(0, 1).toUpperCase() + color.substring(1).toLowerCase();

        TShirt2 t4 = new TShirt2(TShirt2.TShirtSizes.valueOf(size), TShirt2.TShirtColors.valueOf(color), producer, model);
        System.out.println(t4);

        System.out.println();

        if (t3.equals(t4)) {
            System.out.println("To ta sama koszulka!");
        }
    }
}
